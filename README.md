# Spring Portlet MVC Framework w Liferay Portal
##Formularze do wysyłania plików

###Słowo wstępne

Podczas realizacji projektu opartego o portal Liferay pojawił się problem dotyczący przesyłania plików za pośrednictwem dostępnych w portalu formularzy. Dlatego też, narodziła się potrzeba napisania dodatkowego komponentu, który umożliwiłby Upload plików i zapisanie ich wewnątrz infrastruktury Liferay (Documents and Media).

###Instalacja

Instalacja Portletu odbywa się w standardowy dla Liferay sposób tj. poprzez deploy pliku .war do katalogu:

```
#!bash
{home_liferay}/liferay/deploy/

```

###Konfiguracja

####Konfiguracja miejsca zapisów plików

Podstawowa konfiguracja Portletu odbywa się poprzez edycję pliku konfiguracyjnego: portlet.properties.
Sterowanie miejsca (katalogu głównego) utworzenie podkatalogu (nazwa podkatalogu tworzona jest na podstawie aktualnej daty i godziny) w bazie Liferay odbywa się za pośrednictwem parametrów:
```
DLFolder.repositoryId= 
DLFolder.parentFolderId=
```
Parametry te mają odzwierciedlenie w bazie danych w tabeli DLFolder.
DLFolder.repositoryId - Id repozytorium plików
DLFolder.parentFolderId - Id "rodzica" głównego katalogu.

####Uprawnienia do zasobów Liferay

Spring Portlet osadzony jest w infrastrukturze Lifray Portal i w konfiguracji musi uwzględniać uprawnienia stosowane w tej architekturze. Głównym plikiem konfiguracyjnym jest plik resource-actions/default.xml zawierający konfigurację uprawnień modeli:
1. Całego Portletu;
2. com.liferay.portal.kernel.repository.model.Folder;
3. com.liferay.document.library.kernel.model.DLFileEntry;
Konfiguracja uprawnień zgodna z dokumentacją Lifray. Przykład dostępny w tutorialu: [Adding Permissions to Resources ](https://dev.liferay.com/develop/tutorials/-/knowledge_base/7-0/adding-permissions-to-resources)