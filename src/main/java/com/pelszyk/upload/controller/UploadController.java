package com.pelszyk.upload.controller;

import com.liferay.document.library.kernel.exception.FileNameException;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.FileItem;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.util.portlet.PortletProps;
import com.pelszyk.upload.Util.CreateFolderName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;


import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;


/**
 * Created by slawo on 05.05.2017.
 */

@Controller
@RequestMapping("VIEW")
@SessionAttributes("FormFiles")
public class UploadController {

    private static final Logger logger =
            LoggerFactory.getLogger(UploadController.class);
    private static String ROOT_FOLDER_PARENT_ID = PortletProps.get("DLFolder.parentFolderId");
    private static String ROOT_FOLDER_REPOSITORY_ID = PortletProps.get("DLFolder.repositoryId");


    @Autowired
    UploadController() {
        super();
    }


    @RenderMapping
    public ModelAndView welcome() {
        logger.info("Rozpoczęcie sesji zgłoszenia nowego projektu.");
        return new ModelAndView("startForm");
    }

    @RenderMapping(params = "action=formFinish")
    public String formUploadFinish() {
        logger.info("Nowy projekt został zgłoszony do akceptacji");
        return "endForm";
    }

    @ActionMapping(params = "action=uploadStart")
    public void uploadAction(ActionRequest request, ActionResponse response, Model model, SessionStatus status) throws IOException, PortletException, PortalException, SystemException {


        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
       //themeDisplay.getS

        UploadPortletRequest uploadPortletRequest = null;

        uploadPortletRequest = PortalUtil.getUploadPortletRequest(request);

        File file1 = uploadPortletRequest.getFile("uploadedFile");
        if (file1.isFile() == true) {
            try {
                ServiceContext serviceContext = ServiceContextFactory.getInstance(DLFolder.class.getName(), request);
                CreateFolderName createFolderName = new CreateFolderName();
                Folder folder = DLAppServiceUtil.addFolder(Long.parseLong(ROOT_FOLDER_REPOSITORY_ID), Long.parseLong(ROOT_FOLDER_PARENT_ID), createFolderName.getFolderName(), "Repozytorium zgłoszenia", serviceContext);
                logger.info("W katalogu repozytorium " + ROOT_FOLDER_REPOSITORY_ID + " utworzono podfolder nr : " + folder.getFolderId());


                Map<String, FileItem[]> files = uploadPortletRequest.getMultipartParameterMap();
                logger.info("files.size(): " + files.size());
                for (Entry<String, FileItem[]> file2 : files.entrySet()) {
                    FileItem item[] = file2.getValue();

                    for (FileItem fileItem : item) {

                        DLAppServiceUtil.addFileEntry(folder.getRepositoryId(), folder.getFolderId(), fileItem.getFullFileName(),
                                fileItem.getContentType(), fileItem.getFileName(), "Dodano z formularza", "Wersja 1", fileItem.getInputStream(), fileItem.getSize(), serviceContext);
                        logger.info("Do repozytorium: " + folder.getFolderId() + " dodano plik: " + fileItem.getFileName() + " rzomiar: " + fileItem.getSize());


                        //TODO - należy spersonalizosać dyscription
                    }

                }
            } catch (FileNameException e) {
                e.printStackTrace();
            }
            catch (Exception e) {
                e.printStackTrace();
            }

            response.setRenderParameter("action", "formFinish");
        }
    }
}