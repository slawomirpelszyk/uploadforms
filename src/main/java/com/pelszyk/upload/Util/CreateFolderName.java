package com.pelszyk.upload.Util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by slawo on 12.05.2017.
 */
public class CreateFolderName {

    private LocalDateTime localDateTime = LocalDateTime.now();
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    private String localDateTimeForm = localDateTime.format(formatter);

    public CreateFolderName() {
        super();
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }


    public DateTimeFormatter getFormatter() {
        return formatter;
    }


    public String getFolderName() {
        return localDateTimeForm;
    }
}
