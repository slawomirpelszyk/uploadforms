<%--
  Created by IntelliJ IDEA.
  User: slawo
  Date: 06.05.2017
  Time: 14:24
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="fn"
           uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet" %>

<%@page isELIgnored="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="springfmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib prefix="liferay-ui" uri="http://www.springframework.org/tags/form" %>

<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<portlet:defineObjects />



<portlet:actionURL name="start" var="start">
    <portlet:param name="action" value="uploadStart"></portlet:param>
</portlet:actionURL>

<b><spring:message text="<spring:message code='startForm.title.text' text='Prosze dodać plik do formularza'/>" code="startForm.text"/> </b>

<aui:form action="${start}" method="post" modelAttribute="FormFiles" enctype="multipart/form-data" id="Files">



    <aui:input name="uploadedFile" type="file" autoSize="2" onchange="setUploadSize(this)" label="<spring:message code='startForm.label.text' text='Proszę dodać plik'/>">
        <aui:validator name="acceptFiles" errorMessage="Podano niewłaściwy format dokumentu">'pdf,png, doc, docx, txt, jpg, jpeg, cvs, zip, rar, odt, ods, xls, xlsx, ppt, pptx, tif, gif'</aui:validator>
    </aui:input>

    <aui:input name="uploadedFile" type="file" label="<spring:message code='startForm.label.text' text='Proszę dodać plik'/>">
        <aui:validator name="acceptFiles" errorMessage="Podano niewłaściwy format dokumentu">'pdf,png, doc, docx, txt, jpg, jpeg, cvs, zip, rar, odt, ods, xls, xlsx, ppt, pptx, tif, gif'</aui:validator>
    </aui:input>

    <aui:input name="uploadedFile" type="file" label="<spring:message code='startForm.label.text' text='Proszę dodać plik'/>">
        <aui:validator name="acceptFiles" errorMessage="Podano niewłaściwy format dokumentu">'pdf,png, doc, docx, txt, jpg, jpeg, cvs, zip, rar, odt, ods, xls, xlsx, ppt, pptx, tif, gif'</aui:validator>
    </aui:input>

    <aui:input name="uploadedFile" type="file" label="<spring:message code='startForm.label.text' text='Proszę dodać plik'/>">
        <aui:validator name="acceptFiles" errorMessage="Podano niewłaściwy format dokumentu">'pdf,png, doc, docx, txt, jpg, jpeg, cvs, zip, rar, odt, ods, xls, xlsx, ppt, pptx, tif, gif'</aui:validator>
    </aui:input>

    <aui:input name="uploadedFile" type="file" label="<spring:message code='startForm.label.text' text='Proszę dodać plik'/>">
        <aui:validator name="acceptFiles" errorMessage="Podano niewłaściwy format dokumentu">'pdf,png, doc, docx, txt, jpg, jpeg, cvs, zip, rar, odt, ods, xls, xlsx, ppt, pptx, tif, gif'</aui:validator>
    </aui:input>


    <input type="Submit" value="<spring:message code='startForm.submit.text' text='Zapisz plik'/>" class="btn btn-primary btn-large">
</aui:form>
<script>
    $(document).ready(function() {
        $('#Files').formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                avatar: {
                    validators: {
                        notEmpty: {
                            message: 'Please select an image'
                        },
                        file: {
                            extension: 'jpeg,jpg,png',
                            type: 'image/jpeg,image/png',
                            maxSize: 2097152,   // 2048 * 1024
                            message: 'The selected file is not valid'
                        }
                    }
                }
            }
        });
    });
</script>